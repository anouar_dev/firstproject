<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/home/{name}", name="homepage")
     */
   /* public function indexAction(Request $request,$name)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'name' => $name 
            ]);
    }*/
   /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need
        return $this->render(':default:index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            ]);
    }
    /**
     * @Route("/accueil.{_format}", name="accueil",defaults={"_format": "html"},
     *     requirements={
     *         "_format": "html",
     *     })
     */
    public function indexAccueilAction()
    {
        return $this->render(':default:accueil.html.twig');
    }
    /**
     * @Route("{id}/test.{_format}", name="test",defaults={"_format": "html"},
     *     requirements={
     *         "_format": "html",
     *         "id" : "\d+", 
     *     })
     */
    // \d+ = mettre les id qu'en numerique
    public function indexTestAction($id)
    {
        return $this->render(':default:test.html.twig' , array( "id" => $id));
    }
}
