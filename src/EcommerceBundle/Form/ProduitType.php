<?php 


namespace EcommerceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
class ProduitType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('nom', TextType::class)
            ->add('description', TextareaType::class)
            ->add('categorie', TextareaType::class)
            ->add('prix', MoneyType::class)
            ->add('disponible', ChoiceType::class, array(
                'choices'  => array(
                    'Disponible' => true,
                    'Pas Disponible' => false,
                )))
            ->add('image', TextType::class)
            ->add('user', EntityType::class, array(
                'class' => 'UtilisateurBundle\Entity\User',
                'mapped' => false
            ))
            ->add("Ajouter", SubmitType::class);

    }



}
