<?php

namespace EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use EcommerceBundle\Entity\Produits;
use EcommerceBundle\Form\UserType;
use UtilisateurBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use EcommerceBundle\Form\ProduitType;

class ProduitsController extends Controller
{
    /**
     * @Route("/produit",name="produit")
     */
    public function ProduitsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $produit = new Produits();

        $produit->setNom("Asus");
        $produit->setCategorie("PC");
        $produit->setDescription("un nouveau asus");
        $produit->setImage("http://www.republic-of-gamers.fr/images/gallery/asus-eye.jpg");
        $produit->setDisponible("0");
        $produit->setPrix('1300');

       // $em->persist($produit);
       //$em->flush();
        $produits = $em->getRepository('EcommerceBundle:Produits')->findAll();


        return $this->render('EcommerceBundle:Default:produit.html.twig', array("produits" => $produits));
    }

    /**
     * @Route("/ajoutP",name="ajoutP")
     */
    public function testFormAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            echo '<pre>';
            var_dump($form->getData());
            echo '</pre>';
            die;
            return $this->redirectToRoute('produit');
        }
        return $this->render('EcommerceBundle:Default:user.html.twig', array(
            "form" => $form->createView()
        ));

    }

    /**
     * @Route("/ajout",name="ajout")
     */
    public function ProductFormAction(Request $request)
    {
        $Produits = new Produits();
        $form_p = $this->createForm(ProduitType::class, $Produits);
        $form_p->handleRequest($request);

        if ($form_p->isSubmitted() && $form_p->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $Produits->setNom($form_p->getData()->getNom());
            $Produits->setCategorie($form_p->getData()->getCategorie());
            $Produits->setDescription($form_p->getData()->getDescription());
            $Produits->setImage("http://www.republic-of-gamers.fr/images/gallery/asus-eye.jpg");
            $Produits->setDisponible($form_p->getData()->getDisponible());
            $Produits->setPrix($form_p->getData()->getPrix());

            $em->persist($Produits);
            $em->flush();
 
            return $this->redirectToRoute('produit');
        }
        return $this->render('EcommerceBundle:Default:user.html.twig', array(
            "form" => $form_p->createView()
        ));

    }
}
