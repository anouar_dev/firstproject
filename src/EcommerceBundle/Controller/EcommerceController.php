<?php

namespace EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class EcommerceController extends Controller
{
    /**
     * @Route("/ecommerce",name="ecommerce")
     */
    public function indexAction()
    {

        return $this->render('EcommerceBundle:Default:user.html.twig');

    }
}
